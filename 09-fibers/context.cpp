#include <iostream>

#include <boost/context/all.hpp>
#include <boost/asio.hpp>

void ExampleSimple() {
    int a;
    auto source = boost::context::callcc([&a] (auto sink) {
        std::cerr << "Entering continuation" << std::endl;
        int b = 1;
        a = 1;
        while (true) {
            sink = sink.resume();
            int next = a + b;
            a = b;
            b = next;
        }
        return std::move(sink);
    });

    std::cerr << "Starting loop" << std::endl;
    for (int j = 0; j < 10; ++j) {
        std::cout << a << " ";

        source = source.resume();
    }
}

void ExampleStackUnwinding() {
    auto source = boost::context::callcc([] (auto sink) {
        struct LogDestruction {
            ~LogDestruction() {
                std::cerr << "Stack unwinded" << std::endl;
            }
        } Log;

        while (true) {
            sink = sink.resume();
            std::cerr << "Resumed" << std::endl;
        }

        return std::move(sink);
    });

    source = source.resume();
    source = source.resume();
}

void ExampleASIO() {
    boost::asio::io_service io_service;

    boost::context::continuation connection = boost::context::callcc([&] (auto loop) {
        boost::asio::ip::tcp::socket socket(io_service);
        boost::asio::ip::tcp::endpoint endpoint(
            boost::asio::ip::address::from_string("127.0.0.1"), 9999);

        socket.async_connect(endpoint, [&] (const boost::system::error_code& ec) {
            if (ec) {
                connection = connection.resume_with([&] (auto ctx) {
                    loop = std::move(ctx);
                    throw boost::system::system_error(ec);
                });
            } else {
                connection = connection.resume();
            }
        });
        try {
            std::cerr << "Establishing connection" << std::endl;
            loop = loop.resume();
            std::cerr << "Connected" << std::endl;
        } catch (const std::exception& ex) {
            return std::move(loop);
        }

        std::array<char, 5> line;
        boost::asio::async_read(socket, boost::asio::buffer(line), [&] (const boost::system::error_code& ec, size_t size) {
            if (ec) {
                connection = connection.resume_with([&] (auto ctx) {
                    loop = std::move(ctx);
                    throw boost::system::system_error(ec);
                });
            } else {
                connection = connection.resume();
            }
        });
        try {
            std::cerr << "Waiting data" << std::endl;
            loop = loop.resume();
            std::cerr << "Data received" << std::endl;
        } catch (const std::exception& ex) {
            return std::move(loop);
        }

        return std::move(loop);
    });

    io_service.run();
}

int main() {
    ExampleASIO();
    return 0;
}
