* Fibers

** Архитектура Thread Per Connection

*** Плюсы
  1. Просто писать код.
  2. Просто взаимодействовать со сторонними сихронными библиотеками.
  3. Просто отлаживать код.

*** Минусы
  1. Нужна синхронизация для shared объектов.
  2. Тяжело масштабировать на большое количество конкурентных действий.
  3. Сложно прерывать/отменять соединения. 
  4. CPU планировщик используется для планирования I/O.

** Архитектура State Machine

*** Плюсы
  1. Полный контроль над тем в каком порядке выполняются Callback-и.
  2. Синхронизация неявная (не нужно локов).
  3. Просто добавлять в код конкурентные действия.

*** Минусы
  1. Сложно писать и отлаживать код.
  2. Сложно дружить с синхронными библиотеками.

** Threads vs State Machines

 - Мы видели 2 API для работы с сетью: Синхронный и Асинхронный.
 - Две возможных архитектуры сервера: Thread-per-connection и FSM-on-a-single-thread.
 - Есть ли что-то посередине?

** Fibers Context API

fiber MakeFiber(std::function<void()> fn);
fiber* ThisFiber();
void SwitchTo(fiber* other);
void Yield();

** Transforming async code...

void ReadResponse(std::function<void(Msg)> onReponse) {
    async_read(socket, buffer, [onResponse] (error_code error, size_t size) {
        auto msg = Parse(buffer);
        onResponse(msg);
    });
}

** Transforming async code with fibers

void MainLoop() {
    while (true) {
        io_service.run();
    }
}

auto MainFiber = MakeFiber(MainLoop);

Msg ReadResponse() {
    auto me = ThisFiber();
    async_read(socket, buffer, [&me] (error_code error, size_t size) {
        SwitchTo(me);
    });
    Yield();

    return Parse(buffer);
}

** Fiber Context API Extension

fiber MakeFiber(std::function<void()> fn);
fiber* ThisFiber();
void SwitchTo(fiber* other);
void Yield();

void CancelFiber(fiber* other);

// NOTE: not inherited from std::exception
class FiberCanceledException {}; 
