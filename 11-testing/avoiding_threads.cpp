#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <chrono>
#include <thread>

struct Money {};

class ExchangeConnection {
public:
    void Start();
    void Stop();
    ~ExchangeConnection() { Stop(); }

    void Fetch();

    Money GetBid(const std::string& ticker);

private:
    std::map<std::string, Money> Quotes_;

    void ThreadMain() {
        while (true) {
            Fetch();
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
};

TEST(ExchangeServer, Simple) {
    ExchangeConnection conn;

    conn.Fetch();

    EXPECT_GE(conn.GetBid("BTC-USD"), 6000);
}