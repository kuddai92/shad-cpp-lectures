#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <chrono>
#include <deque>

struct TimeZone {};
struct TimePoint {};
bool operator == (const TimePoint& lhs, const TimePoint& rhs) {
    return true;
}

class Service {
public:
    // 50 parameters
    Service() {}

    // 50 methods
    // &date=xxxxxxxx
    std::string HandleRequest(const std::string& request);

private:
    TimeZone TimeZone_;
    // 50 fields

    // 50 private methods
    TimePoint ParseDate(const std::string& date);
};

TEST(ServiceTest, TestTimeParsing) // DO NOT DO THIS
{
    Service s; // <- Need to create all dependencies as well
    // Make fake request
    // Check response
}

// Better
TimePoint ParseDate(const std::string& date, const TimeZone& timeZone) {
    return {};
}

TEST(TimeTest, TestTimeParsing)
{
    TimeZone timeZone;
    EXPECT_EQ(TimePoint{}, ParseDate("00:01+T0400", timeZone));
}

//

struct FileLogger {
    void Write(const std::string& entry);
};

class LoggingService {
public:
    LoggingService(FileLogger* logger) : Logger_(logger) {}

private:
    FileLogger* Logger_ = nullptr;
};




struct ILogger {
    virtual void Write(const std::string& entry) = 0;
    virtual std::string GetRecord(size_t index) = 0;
};

struct LoggerStub : public ILogger {
    virtual void Write(const std::string& entry) {
        // Nothing here, just stub.
    }
};

class BetterLoggingService {
public:
    BetterLoggingService(ILogger* logger) : Logger_(logger) {}

    void DoThis() {
        Logger_->Write("this");
    }

    void DoThat() {
        // Code <----
        Logger_->Write("that");
        // Code
    }

    void Reload() {
        Logger_->GetRecord(0);
    }

private:
    ILogger* Logger_ = nullptr;
};

TEST(TestLoggingService, SomeFunctionalty) {
    LoggerStub stub;
    BetterLoggingService service(&stub);

    service.DoThis();

    service.DoThat();
}

struct LoggerFake : public ILogger {
    std::vector<std::string> Entries;

    virtual void Write(const std::string& entry) {
        Entries.push_back(entry);
    }

    std::string GetRecord(size_t index) {
        return Entries[index];
    }
};

TEST(TestLoggingService, Logging) {
    LoggerFake fake;
    BetterLoggingService service(&fake);

    service.DoThis();
    EXPECT_EQ(fake.Entries, std::vector<std::string>{"this"});

    service.DoThat();
    EXPECT_EQ(fake.Entries, (std::vector<std::string>{"this", "that"}));
}

struct LoggerMock : public ILogger {
    MOCK_METHOD1(Write, void(const std::string& entry));
    MOCK_METHOD1(GetRecord, std::string(size_t index));
};

using namespace ::testing;

TEST(TestLoggingService, LoggingWithMock) {
    LoggerMock mock;
    BetterLoggingService service(&mock);

    EXPECT_CALL(mock, Write("this"));
    service.DoThis();

    EXPECT_CALL(mock, Write("that"));
    service.DoThat();

    EXPECT_CALL(mock, GetRecord(0))
        .WillOnce(Return("this"));
    service.Reload();
}