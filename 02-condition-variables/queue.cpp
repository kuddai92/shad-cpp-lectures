#include <mutex>
#include <condition_variable>
#include <queue>

class FixedQueue {
public:
    FixedQueue(int max_size) : max_size_(max_size) {}
    int pop() {
        std::unique_lock<std::mutex> guard(lock_);
        empty_.wait(guard, [this] {
            return !elements_.empty();
        });
        full_.notify_one();
        int front = elements_.front();
        elements_.pop();
        return front;
    }
    void push(int element) {
        std::unique_lock<std::mutex> guard(lock_);
        full_.wait(guard, [this] {
            return elements_.size() < max_size_;
        });
        empty_.notify_one();
        elements_.push(element);
    }

private:
    std::mutex lock_;
    std::condition_variable empty_, full_;
    int max_size_;
    std::queue<int> elements_;
};

int main() {
    return 0;
}
